// praktek filter Method

var mobil = [
{merk: "BWW", 
warna: "merah",
tipe: "sport car"}, 

{merk: "Toyota", 
warna: "hitam",
tipe: "MVP"}, 

{merk: "Honda", 
warna: "Abu abu",
tipe: "SUV"}]

var arrayMobilFilter = mobil.filter(function(item) {
    return item.tipe != "MVP";
});

console.log(arrayMobilFilter);