// Java script Array, Function dan Object

// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1.Tokek"];
var daftarTerurut = [];
var j = 0;

for(var i = 0; i < 5; i++){
    j = (daftarHewan[i].substring(0,1)) - 1;
    daftarTerurut[j] = daftarHewan[i];
}

for(var i = 0; i < 5; i++){
    console.log(daftarTerurut[i]);
}

// soal 2


function introduce(data){
    var pesan = "";
    pesan = "Nama saya " + data.name + ", umur saya " + data.age + 
    " tahun, alamat saya di " + data.address + ", dam saya punya hobby yaitu " + data.hobby;
  
    return pesan;
}

var data = {name : "John", age : 30, address : "Jalan Pelesiran", hobby : "Gaming"};
var perkenalan = introduce(data);
console.log(perkenalan);

//soal 3 
var hitung_huruf_local = function(parameter1){
    
    var j = 0;
    for(var i = 0; i < parameter1.length; i++){
        // if (parameter1.substring(i, i+1) == "a" || "A" || "i" || "I" || "u" || "U" || "e" || "E" || "o" || "O" ){
        if ((parameter1.substring(i, i+1) == "a") || 
            (parameter1.substring(i, i+1) == "A") ||
            (parameter1.substring(i, i+1) == "i") ||
            (parameter1.substring(i, i+1) == "I") ||
            (parameter1.substring(i, i+1) == "u") ||
            (parameter1.substring(i, i+1) == "U") ||
            (parameter1.substring(i, i+1) == "e") ||
            (parameter1.substring(i, i+1) == "E") ||
            (parameter1.substring(i, i+1) == "o") ||
            (parameter1.substring(i, i+1) == "O")){        

            j++;
        }
    }
    return j;
}

var hitung_1 = hitung_huruf_local("Muhammad");
var hitung_2 = hitung_huruf_local("Iqbal");
console.log(hitung_1, hitung_2); 


// soal 4

var hitung = function(parameterHitung){
    return (-2 + (2 * parameterHitung));
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(4));
console.log(hitung(5));
