// Soal 1. Function Penghitung Jumlah Kata

var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok";
var kalimat_2 = "Saya Iqbal";

var jumlah_kata = function(kirim){
    var jumlah = 0
    // console.log(kirim);
    for (var i = 0; i < kirim.length; i++){
        // console.log(i);
        // console.log(kirim.substring(i, i+1));
        if (i == 0){
            jumlah++;
        }
        if (kirim.substring(i, i+1) == " "){
            jumlah++;
        }
    }
    return jumlah;
}

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));


// soal 2 Function Penghasil Tanggal Hari Esok


var next_date = function(tanggal, bulan, tahun){
    var tanggalBesok;
    var maxHari = 0;

    if(bulan == 1 ||
        bulan == 3 ||
        bulan == 5 ||
        bulan == 7 ||
        bulan == 8 ||
        bulan == 10 ||
        bulan == 12 ){ 

        maxHari = 31;
        if (tanggal == maxHari){
            if(bulan == 12){  // akhir hatun 31 des
                tanggal = 1;
                bulan = 1;
                tahun++;
            }
            else {   // akhir bulan
                tanggal = 1;
                bulan++;              
            }
        }
        else {
            tanggal++;  // pindah hari biasa
        }
    }
    
    if(bulan == 2){ 
        if ((tahun % 4) == 0){ // kabisat
            maxHari = 29;
        } else {
            maxHari = 28;     
  
        }
        if (tanggal == maxHari){
            // console.log(maxHari);
            tanggal = 1;
            bulan++;
        }
        else {
            
            tanggal++;  // pindah hari biasa
        }
    }
    
    if(bulan == 4 ||
        bulan == 6 ||
        bulan == 9 ||
        bulan == 11 ){ 
        maxHari = 30;
        if (tanggal == maxHari){
            tanggal = 1;
            bulan++;
        }
        else {
            tanggal++;  // pindah hari biasa
        }
    }

    tanggalBesok = tanggal + " " + stringBulan(bulan) + " " + tahun ;

    return tanggalBesok;
}

var stringBulan = function(iBulan){
    var sbulan = "";
    switch(iBulan){
        case 1: {sbulan = "Januari"; break}
        case 2: {sbulan = "Februari"; break}
        case 3: {sbulan = "Maret"; break}
        case 4: {sbulan = "April"; break}
        case 5: {sbulan = "Mei"; break}
        case 6: {sbulan = "Juni"; break}
        case 7: {sbulan = "Juli"; break}
        case 8: {sbulan = "Agustus"; break}
        case 9: {sbulan = "September"; break}
        case 10: {sbulan = "Oktober"; break}
        case 11: {sbulan = "November"; break}
        case 12: {sbulan = "Desember"; break}
    }

    return sbulan;
}

var tanggal = 29;
var bulan = 2;
var tahun = 2020;
console.log(next_date(tanggal, bulan, tahun));

var tanggal = 28
var bulan = 2
var tahun = 2021

console.log(next_date(tanggal , bulan , tahun ));

var tanggal = 31
var bulan = 12
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun ));

var tanggal = 20
var bulan = 5
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun ));




