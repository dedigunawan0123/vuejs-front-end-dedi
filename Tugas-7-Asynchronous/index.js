// soal callback - Asynchronous

var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'Komik', timeSpent: 1000}
]

for(var i = 0; i < books.length; i++){

    console.log("Masuk " + books[i].name)
    // console.log (`${readBooks.readBooks((books[i].timeSpent, books[i].name))}`)
}


// soal 2 promise

var readBooksPromise = require('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

console.log(`${readBooksPromise.readBooksPromise(books[2].timeSpent, books[2].name)}`)


