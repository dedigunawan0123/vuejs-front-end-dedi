var sentences = "Javascript"
console.log(sentences[0])
console.log(sentences[2])

var word = "Javascript is awesome"
console.log(word.length) //21

// var tempWord = "i am a string";
console.log('i am a string'.charAt(3)); // 'm'

// tentang concat
var string1 = "good";
var string2 = "luck";
console.log(string1.concat(string2));  // goodluck

// tentang indexOf : untuk mengembalikan index dari string/karakter yang dicari
var text1 = "dung dung ces!";
console.log(text1.indexOf('dung')); // 0
console.log(text1.indexOf('u'));    // 1
console.log(text1.indexOf('cos'));  // -1

// tentang substring : mengembalikan potongan string mulai dari index pada parameter
// pertama sampai index parameter kedua
var car1 = 'Lykan Hypersport';
var car2 = car1.substring(6);
console.log(car2);  // Hypersport

// tentang substr : untuk mendapatkan potongan string sebanyak n kali
var motor1 = 'zelda motor';
var motor2 = motor1.substr(2, 2);
console.log(motor2);

// toUpperCase()
// untuk mengembalikan string 
var letter = "This Letter Is For You";
var upper = letter.toUpperCase();
console.log(upper);  // THIS LETTER IS FOR YOU

// toLowerCase
// untuk mengubah semua huruf menjadi huruf kecil
var letter = "This Letter Is For You";
var lower = letter.toLowerCase();
console.log(lower);

// trim
// untuk menghilangkan koma/whitespace
var username = ' administator ';
var newUsername = username.trim();
console.log(newUsername) // 'administrator'