// Ini file untuk Conditional dan Loop

// soal 1
var nilai;
var index;

nilai = 90;
if (nilai >= 85) {
    index = "A";
}
else if (nilai >= 75 & nilai < 85){
    index = "B";
}
else if (nilai >= 65 & nilai < 75){
    index = "C";
}
else if ( nilai >= 55 & nilai < 65){
    index = "D";
}
else {
    index = "E";
}

console.log("Nilai = " + nilai + ", index = " + index);
