// soal hari ke 1 week ke 2

// luas persegi panjang

var luasPersegiPanjang = (pPanjang, pLebar) => {return pPanjang * pLebar}

var npanjang = 5;
var nlebar = 4;

console.log("Luas persegi panjang : " + luasPersegiPanjang(npanjang, nlebar));


// soal Arrow Function dan object literal

var newFunction = (firstName, lastName) =>{
    return{
        firstName,
        lastName,
        fullName : function(){
            console.log(firstName + " " + lastName)
        }
    }
}

// Driver code
newFunction("William", "Imoh").fullName();


// soal 3 Destructing 

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalana Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)

// soal 4 Array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combine = west.concat(east)

const combine = [...west, ...east]

//Driver code
console.log(combine)

// soal 5 Template literal

const planet = 'earth'
const view = 'glass'

// ES5
var before = 'Lorem' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
//ES6
console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`)

