// Ini file untuk Pengenalan javascript

// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// hasilnya harus menjadi "saya senang belajar JAVASCRIPT"
var gabungan = pertama.substring(0, 4);
var gabungan = gabungan.concat(" ").concat(pertama.substring(12, 18));
var gabungan = gabungan.concat(" ").concat(kedua.substring(0, 7));
var gabungan = gabungan.concat(" ").concat((kedua.substring(8, 17)).toUpperCase());
console.log(gabungan);

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var iKataPertama = Number(kataPertama);
var iKataKedua = Number(kataKedua);
var iKataKetiga = Number(kataKetiga);
var iKataKeempat = Number(kataKeempat);

var hasilHitung = iKataPertama + iKataKedua * iKataKetiga + iKataKeempat;

console.log(hasilHitung);

// soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua; // do your own
var kataKetiga; // do your own
var kataKeempat; // do your own
var kataKelima; // do your own

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);




